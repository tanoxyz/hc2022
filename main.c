
#include "stuff.c"


#if 0

int
main(int argc, const char **argv) {
    if (argc != 2) return -1;
    String file_contents = Read_all_file(argv[1]);
    #define HM_SIZE 4096
    static U32    count[HM_SIZE]   = {0};
    static String words[HM_SIZE]   = {0};
    static I32    indices[HM_SIZE] = {0};
    I32 word_count = 0;
    for (I64 i = 0; i < HM_SIZE; i+=1) indices[i] = -1;
    HashMap hm = Make_HashMap(HM_SIZE, 1234);
    for (I64 index = 0; index < file_contents.len; index+=1) {
        while (index < file_contents.len && !Is_alpha(file_contents.d[index])) {
            index+=1;
        }
        if (index >= file_contents.len) break;

        I64 begin = index;
        while (index < file_contents.len && Is_alpha(file_contents.d[index])) {
            index+=1;
        }
        String word = Substring(file_contents, begin, index);
        I64 key_index = HM_At_str(&hm, word, true);
        if (indices[key_index] == -1) {
            indices[key_index] = word_count;
            words[word_count]  = word;
            count[word_count]  = 1;
            word_count += 1;
        }
        else {
            count[indices[key_index]] += 1;
        }
    }

    for (I64 word_index = 0; word_index < word_count; word_index+=1) {
        printf("%.*s %d\n", (int)words[word_index].len, words[word_index].d, count[word_index]);
    }
    return 0;
}

#endif

#if 0

typedef struct {
    char d[16];
} Ingredient;

typedef struct {
    I8  total_liked;
    I32 liked[5];
    I8  total_disliked;
    I32 disliked[5];
} Client;

#define MAX_INGREDIENTS 100000
#define MAX_CLIENTS     20000

I32 ingredients_count = 0;
Ingredient ingredients_storage[MAX_INGREDIENTS]        = {0};
I32        ingredients_indices[MAX_INGREDIENTS]        = {0};
I32        ingredients_times_liked[MAX_INGREDIENTS]    = {0};
I32        ingredients_times_disliked[MAX_INGREDIENTS] = {0};
HashMap    ingredients_hm = {0};

I32 clients_count = 0;
Client     clients_storage[MAX_CLIENTS] = {0};

I32 solution_count = 0;
U8  ingredient_picked[MAX_INGREDIENTS] = {0};
I32 solution[MAX_INGREDIENTS] = {0};


bool
Ingredient_less(I32 l, I32 r) {
    return (ingredients_times_disliked[l] < ingredients_times_disliked[r]);
}

static I32
HM_At_ing(Ingredient ing) {
    I64 result = HM_At_(&ingredients_hm, &ing, sizeof(Ingredient), true);
    ASSERT(result >= 0);
    return (I32)result;
}

static I32
Compute_score(void) {
    U8 *picked = ingredient_picked;
    I32 score = 0;
    for (I32 client_index = 0; client_index < clients_count; client_index+=1) {
        bool should_continue = false;
        Client client = clients_storage[client_index];
        for (I8 i = 0; i < client.total_liked; i+=1) {
            if (!picked[client.liked[i]]) {
                should_continue = true;
                break;
            }
        }
        if (should_continue) continue;
        for (I8 i = 0; i < client.total_disliked; i+=1) {
            if (picked[client.disliked[i]]) {
                should_continue = true;
                break;
            }
        }
        if (should_continue) continue;
        score += 1;
    }
    return score;
}

int
main(int argc, const char **argv) {

    UT_Init();

    const char *out_filename = NULL;
    if (argc == 2) {

    }
    else if (argc == 3) {
        out_filename = argv[2];
    }

    ingredients_hm = Make_HashMap(MAX_INGREDIENTS, 1234);
    for (I32 i = 0; i < MAX_INGREDIENTS; i+=1) ingredients_indices[i] = -1;

    String file_contents = Read_all_file(argv[1]);

    I64 index = 0;
    for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
    I64 begin = index;
    for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
    clients_count = (I32)STR_To_I64(Substring(file_contents, begin, index));
    ASSERT(clients_count <= MAX_CLIENTS);

    for (I32 client_index = 0;  client_index < clients_count; client_index+=1) {
        ASSERT(index < file_contents.len);
  
        Client *client = &clients_storage[client_index];


        {
            for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1) {}
            // Get the total of liked ingredients
            I64 begin = index;
            for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1) {}
            I8 total_liked = (I8)STR_To_I64(Substring(file_contents, begin, index));

            // Get the liked ingredients
            for (I8 ingredient_index = 0; ingredient_index < total_liked; ingredient_index+=1) {
                for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1) {}
                Ingredient ing = {0};
                char *ing_str  = ing.d;
                for (I8 char_index = 0; index < file_contents.len && Is_alphanumeric(file_contents.d[index]); index+=1, char_index+=1) {
                    ASSERT(char_index < 16);
                    ing_str[char_index] = file_contents.d[index];
                }
                I32 hm_index = HM_At_ing(ing);
                if (ingredients_indices[hm_index] == -1) {
                    ingredients_indices[hm_index]          = ingredients_count;
                    ingredients_storage[ingredients_count] = ing;
                    ingredients_count += 1;
                }
                ingredients_times_liked[ingredients_indices[hm_index]] += 1;
                client->liked[ingredient_index] = ingredients_indices[hm_index];
            }
            client->total_liked = total_liked;
        }

        {
            for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1) {}
            // Get the total of disliked ingredients
            I64 begin = index;
            for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
            I8 total_disliked = (I8)STR_To_I64(Substring(file_contents, begin, index));

            // Get the disliked ingredients
            for (I8 ingredient_index = 0; ingredient_index < total_disliked; ingredient_index+=1) {
                for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1) {}
                Ingredient ing = {0};
                char *ing_str  = ing.d;
                for (I8 char_index = 0; index < file_contents.len && Is_alphanumeric(file_contents.d[index]); index+=1, char_index+=1) {
                    ASSERT(char_index < 16);
                    ing_str[char_index] = file_contents.d[index];
                }
                I32 hm_index = HM_At_ing(ing);
                if (ingredients_indices[hm_index] == -1) {
                    ingredients_indices[hm_index]          = ingredients_count;
                    ingredients_storage[ingredients_count] = ing;
                    ingredients_count += 1;
                }
                ingredients_times_disliked[ingredients_indices[hm_index]] += 1;
                client->disliked[ingredient_index] = ingredients_indices[hm_index];
            }
            client->total_disliked = total_disliked;
        }
    }

#if 0
    for (I64 client_index = 0; client_index < clients_count; client_index+=1) {
        printf("Liked: ");
        Client *client = &clients_storage[client_index];
        for (I8 liked_index = 0; liked_index < client->total_liked; liked_index+=1) {
            Ingredient ing = ingredients_storage[client->liked[liked_index]];
            printf("%s", ing.d);
            if (liked_index < client->total_liked-1) printf(" ");
        }
        printf("\nDisiked: ");
        for (I8 disliked_index = 0; disliked_index < client->total_disliked; disliked_index+=1) {
            Ingredient ing = ingredients_storage[client->disliked[disliked_index]];
            printf("%s", ing.d);
            if (disliked_index < client->total_disliked-1) printf(" ");
        }
        printf("\n");
    }
#endif


    I64 time_start = UT_Time();

    solution_count = ingredients_count;
    for (I32 i = 0; i < ingredients_count; i+=1) {
        ingredient_picked[i] = 1;
        solution[i] = i;
    }
    Quicksort_I32(solution, solution_count, Ingredient_less);

    I32 best_score = Compute_score();
    for (I32 sol_index = solution_count-1; sol_index>=0; sol_index-=1) {
        ingredient_picked[solution[sol_index]] = 0;
        I32 new_score = Compute_score();
        if (new_score >= best_score) {
            best_score = new_score;
        }
        else {
            ingredient_picked[solution[sol_index]] = 1;
        }
    }

    solution_count = 0;
    for (I32 i = 0; i < ingredients_count; i+=1) {
        if (ingredient_picked[i]) {
            solution[solution_count] = i;
            solution_count += 1;
        }
    }

    I64 time_end = UT_Time();

    printf("Time elapsed %.4fms\n", 1e-6f*(float)(time_end-time_start));

#if 0
    for (I32 i = 0; i < solution_count; i+=1) {
        printf("Ingredient[%d]: %s liked %d times disliked %d times\n",
                solution[i],
                ingredients_storage[solution[i]].d,
                ingredients_times_liked[solution[i]],
                ingredients_times_disliked[solution[i]]);
    }
#endif


    FILE *out_file = stdout;

    if (out_filename) {
        out_file = fopen(out_filename, "w");
        if (!out_file) {
            LOG_ERROR("Cannot open file %s\n", out_filename);
            return -1;
        }
    }

    fprintf(out_file, "%d ", solution_count);
    for (I32 i = 0; i < solution_count; i+=1) {
        fprintf(out_file, "%s", ingredients_storage[solution[i]].d);
        if (i < solution_count-1) fprintf(out_file, " ");
    }
    fprintf(out_file, "\n");
    if (out_filename) {
        fclose(out_file);
    }

    printf("Score: %d  Maximum: %d\n", Compute_score(), clients_count);
    printf("Picked: %d  ingredients of %d\n", solution_count, ingredients_count);

    return 0;
}

#endif

#define MAX_CONTRIBUTORS 100000
#define MAX_PROJECTS     100000
#define MAX_SKILLS       1000000
#define MAX_PLANS        1000000

typedef struct {
    String name;
} Skill;

typedef struct {
    String name;
    I32 total_skills;
    I32 skills[100];
    I32 skill_levels[100];
} Contributor;

typedef struct {
    String name;
    I32 days_for_completion;
    I32 score;
    I32 best_before;
    I32 total_skills;
    I32 skills[100];
    I32 skill_levels[100];
} Project;


I32     skills_count = 0;
Skill   all_skills[MAX_SKILLS] = {0};
I32     skills_indices[MAX_SKILLS] = {0};
HashMap skills_hm = {0};

static I32
HM_At_skill(Skill skl) {
    I64 result = HM_At_str(&skills_hm, skl.name, true);
    ASSERT(result >= 0);
    return (I32)result;
}

I32 contributors_count = 0;
I32 projects_count     = 0;

Contributor all_contributors[MAX_CONTRIBUTORS] = {0};
Contributor all_contributors_tmp[MAX_CONTRIBUTORS] = {0};
I32         contributors_working_until[MAX_CONTRIBUTORS] = {0};
Project     all_projects[MAX_PROJECTS]         = {0};
I32         sorted_projects[MAX_PROJECTS]      = {0};

typedef struct {
    I32 project;
    I8 total_contributors;
    I32 contributors[100];
} Plan;

I32 plans_count = 0;
Plan plans[2][MAX_PLANS] = {0};
Plan *all_plans = NULL;

static bool
Project_Less_best_before(I32 l, I32 r) {
    return (all_projects[l].best_before < all_projects[r].best_before);
}

static bool
Project_Less_score(I32 l, I32 r) {
    return (all_projects[l].score > all_projects[r].score);
}

static bool
Project_Less_days_to_complete(I32 l, I32 r) {
    return (all_projects[l].days_for_completion < all_projects[r].days_for_completion);
}

void
Compute_plans() {

    for (I32 ci = 0; ci < contributors_count; ci += 1) {
        all_contributors_tmp[ci] = all_contributors[ci];
    }

    plans_count = 0;
    for (I32 pi = 0; pi < projects_count; pi+=1) {
        Project *p = &all_projects[sorted_projects[pi]];
        I32 plan_contributors = 0;
        for (I32 si = 0; si < p->total_skills; si+=1) {
            bool found = false;
            I32 skill_id       = p->skills[si];
            I32 level_required = p->skill_levels[si];

            // Check if a contributar has that skill
#if 0
            bool mentor_found = false;
            for (I32 pci = 0; !mentor_found && pci < plan_contributors; pci+=1) {
                Contributor *c = &all_contributors_tmp[pci];
                for (I32 csi = 0; !found && csi < c->total_skills; csi+=1) {
                    if (skill_id == c->skills[csi] && level_required <= c->skill_levels[csi]) {
                        mentor_found = true;
                        break;
                    }
                }
            }
            if (mentor_found) level_required -= 1;
#endif

            for (I32 ci = 0; !found && ci < contributors_count; ci+=1) {
                Contributor *c = &all_contributors_tmp[ci];

                // Check if the contributor isn't aready in the project
                bool should_continue = false;
                for (I32 pci = 0; pci < plan_contributors; pci+=1) {
                    if (all_plans[plans_count].contributors[pci] == ci) {
                        should_continue = true;
                        break;
                    }
                }
                if (should_continue) continue;

                    // Check if the contributor has the required skill
                for (I32 csi = 0; !found && csi < c->total_skills; csi+=1) {
                    if (skill_id == c->skills[csi] && level_required <= c->skill_levels[csi]) {
                        found = true;
                    }
                }
                if (found) {
                    all_plans[plans_count].contributors[si] = ci;
                    plan_contributors += 1;
                }
            }
        }

        if (plan_contributors == p->total_skills) {
            all_plans[plans_count].project = sorted_projects[pi];
            all_plans[plans_count].total_contributors = p->total_skills;
            plans_count += 1;
        }
    }
}


I64
Compute_score() {
    I64 score = 0;

    for (I32 ci = 0; ci < contributors_count; ci+=1) contributors_working_until[ci] = 0;

    for (I32 pi = 0; pi < plans_count; pi+=1) {
        Plan *plan = &all_plans[pi];
        Project *p = &all_projects[plan->project];

        I32 starts_at_day = 0;
        for (I32 ci = 0; ci < plan->total_contributors; ci+=1) {
            I32 contributor = plan->contributors[ci];
            I32 working_until = contributors_working_until[contributor];
            if (working_until > starts_at_day) starts_at_day = working_until;
        }

        I32 day_finished = starts_at_day+p->days_for_completion;
        for (I32 ci = 0; ci < plan->total_contributors; ci+=1) {
            I32 contributor = plan->contributors[ci];
            contributors_working_until[contributor] = day_finished;
        }

        if (day_finished < p->best_before) {
            score += p->score;
        }
        else {
            I64 p_score = p->score - (day_finished - p->best_before);
            if (p_score > 0) {
                score += p_score;
            }
        }
        ASSERT(score >= 0);
    }
    return score;
}

int
main(int argc, const char **argv) {

    UT_Init();

    const char *out_filename = NULL;
    if (argc == 2) {

    }
    else if (argc == 3) {
        out_filename = argv[2];
    }

    skills_hm = Make_HashMap(MAX_SKILLS, 1234);
    for (I32 i = 0; i < MAX_SKILLS; i+=1) skills_indices[i] = -1;

    String file_contents = Read_all_file(argv[1]);

    I64 index = 0;
    for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
    I64 begin = index;
    for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
    contributors_count = (I32)STR_To_I64(Substring(file_contents, begin, index));
    ASSERT(contributors_count <= MAX_CONTRIBUTORS);

    for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
    begin = index;
    for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
    projects_count = (I32)STR_To_I64(Substring(file_contents, begin, index));
    ASSERT(projects_count <= MAX_PROJECTS);

    // Get the contributors
    for (I32 contributor_index = 0;  contributor_index < contributors_count; contributor_index+=1) {
        ASSERT(index < file_contents.len);
  
        Contributor *contributor = &all_contributors[contributor_index];

        // Name of contributor
        for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
        begin = index;
        for (;index < file_contents.len && Is_alphanumeric(file_contents.d[index]); index+=1);
        contributor->name = Substring(file_contents, begin, index);

        // Number of skills
        for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
        begin = index;
        for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
        contributor->total_skills = (I32)STR_To_I64(Substring(file_contents, begin, index));

        // Set the skills and skills levels
        {
            for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1) {}

            // Get each skill and level
            for (I8 skill_index = 0; skill_index < contributor->total_skills; skill_index+=1) {
                for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1) {}
                Skill skill = {0};
                begin = index;
                for (;
                    (index < file_contents.len) &&
                    (Is_alphanumeric(file_contents.d[index]) ||
                     file_contents.d[index] == '-' ||
                     file_contents.d[index] == '+');
                    index+=1) {}
                skill.name = Substring(file_contents, begin, index);

                I32 hm_index = HM_At_skill(skill);
                if (skills_indices[hm_index] == -1) {
                    skills_indices[hm_index] = skills_count;
                    all_skills[skills_count] = skill;
                    skills_count += 1;
                }
                contributor->skills[skill_index] = skills_indices[hm_index];

                for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
                begin = index;
                for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
                contributor->skill_levels[skill_index] = (I32)STR_To_I64(Substring(file_contents, begin, index));
            }
        }
    }

    // Get the projects
    for (I32 project_index = 0;  project_index < projects_count; project_index+=1) {
        ASSERT(index < file_contents.len);
  
        Project *project = &all_projects[project_index];

        // Name of the project
        for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
        begin = index;
        for (;index < file_contents.len && Is_alphanumeric(file_contents.d[index]); index+=1);
        project->name = Substring(file_contents, begin, index);

        // Number of days to complete
        for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
        begin = index;
        for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
        project->days_for_completion = (I32)STR_To_I64(Substring(file_contents, begin, index));

        // Score
        for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
        begin = index;
        for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
        project->score = (I32)STR_To_I64(Substring(file_contents, begin, index));

        // Best before
        for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
        begin = index;
        for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
        project->best_before = (I32)STR_To_I64(Substring(file_contents, begin, index));

        // Total skills
        for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
        begin = index;
        for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
        project->total_skills = (I32)STR_To_I64(Substring(file_contents, begin, index));

        // Set the skills and skills levels
        {
            for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1) {}

            // Get each skill and level
            for (I8 skill_index = 0; skill_index < project->total_skills; skill_index+=1) {
                for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1) {}
                Skill skill = {0};
                begin = index;
                for (;
                    (index < file_contents.len) &&
                    (Is_alphanumeric(file_contents.d[index]) ||
                     file_contents.d[index] == '-' ||
                     file_contents.d[index] == '+');
                    index+=1) {}
                skill.name = Substring(file_contents, begin, index);

                I32 hm_index = HM_At_skill(skill);
                ASSERT (skills_indices[hm_index] != -1);
                project->skills[skill_index] = skills_indices[hm_index];

                for (;index < file_contents.len && Is_space(file_contents.d[index]); index+=1);
                begin = index;
                for (;index < file_contents.len && Is_numeric(file_contents.d[index]); index+=1);
                project->skill_levels[skill_index] = (I32)STR_To_I64(Substring(file_contents, begin, index));

            }
        }
    }

#if 0
    for (I32 contributor_index=0; contributor_index < contributors_count; contributor_index+=1) {
        Contributor *c = &all_contributors[contributor_index];
        printf("Name: %.*s\n", (int)c->name.len, c->name.d);
        for (I32 skill_index=0; skill_index < c->total_skills; skill_index+=1) {
            Skill s = all_skills[c->skills[skill_index]];
            printf("%.*s level: %d\n", (int)(s.name.len), s.name.d, (int)c->skill_levels[skill_index]);
        }
        printf("\n");
    }

    for (I32 project_index=0; project_index < projects_count; project_index+=1) {
        Project *p = &all_projects[project_index];
        printf("Name: %.*s,  days for completion: %d   score: %d   best before: %d\n",
                (int)p->name.len, p->name.d,
                (int)p->days_for_completion,
                (int)p->score,
                (int)p->best_before);
        for (I32 skill_index=0; skill_index < p->total_skills; skill_index+=1) {
            Skill s = all_skills[p->skills[skill_index]];
            printf("%.*s level: %d\n", (int)(s.name.len), s.name.d, (int)p->skill_levels[skill_index]);
        }
        printf("\n");
    }
#endif

    I64 time_start = UT_Time();

    I64 best_score = 0;
    I64 new_score  = 0;

    Plan *best_plan     = plans[0];
    I32 best_plan_count = 0;
    Plan *current_plan = plans[1];
    all_plans = current_plan;

    for (I32 pi = 0; pi < projects_count; pi+=1) sorted_projects[pi] = pi;
    Compute_plans();
    new_score = Compute_score();
    if (new_score > best_score) {
        best_score = new_score;
        Plan *tmp = best_plan;
        best_plan      = current_plan;
        best_plan_count = plans_count;
        current_plan = tmp;
        all_plans = current_plan;
    }

    for (I32 pi = 0; pi < projects_count; pi+=1) sorted_projects[pi] = pi;
    Quicksort_I32(sorted_projects, projects_count, Project_Less_best_before);
    Compute_plans();
    new_score = Compute_score();
    if (new_score > best_score) {
        best_score = new_score;
        Plan *tmp = best_plan;
        best_plan = current_plan;
        current_plan = tmp;
        best_plan_count = plans_count;
        all_plans = current_plan;
    }

    for (I32 pi = 0; pi < projects_count; pi+=1) sorted_projects[pi] = pi;
    Quicksort_I32(sorted_projects, projects_count, Project_Less_score);
    Compute_plans();
    new_score = Compute_score();
    if (new_score > best_score) {
        best_score = new_score;
        Plan *tmp = best_plan;
        best_plan = current_plan;
        best_plan_count = plans_count;
        current_plan = tmp;
        all_plans = current_plan;
    }

    for (I32 pi = 0; pi < projects_count; pi+=1) sorted_projects[pi] = pi;
    Quicksort_I32(sorted_projects, projects_count, Project_Less_days_to_complete);
    Compute_plans();
    new_score = Compute_score();
    if (new_score > best_score) {
        best_score = new_score;
        Plan *tmp = best_plan;
        best_plan = current_plan;
        best_plan_count = plans_count;
        current_plan = tmp;
        all_plans = current_plan;
    }

    all_plans = best_plan;
    plans_count = best_plan_count;

    I64 time_end = UT_Time();

    printf("Time elapsed %.4fms\n", 1e-6f*(float)(time_end-time_start));

    FILE *out_file = stdout;

    if (out_filename) {
        out_file = fopen(out_filename, "w");
        if (!out_file) {
            LOG_ERROR("Cannot open file %s\n", out_filename);
            return -1;
        }
    }

    fprintf(out_file, "%d\n", plans_count);
    for (I32 pi = 0; pi < plans_count; pi+=1) {
        Project *p = &all_projects[all_plans[pi].project];
        fprintf(out_file, "%.*s\n", (int)p->name.len, p->name.d);
        for (I32 ci = 0; ci < all_plans[pi].total_contributors; ci+=1) {
            Contributor *c = &all_contributors[all_plans[pi].contributors[ci]];
            fprintf(out_file, "%.*s", (int)c->name.len, c->name.d);
            if (ci < all_plans[pi].total_contributors-1) fprintf(out_file, " ");
        }
        if (pi < plans_count-1) fprintf(out_file, "\n");
    }

    fprintf(out_file, "\n");
    if (out_filename) {
        fclose(out_file);
    }

    printf("Score: %d\n", (int)best_score);

    return 0;
}


