
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define __thread thread_local
#define global static

typedef _Bool bool;
enum {
    false = 0,
    true  = 1,
};
typedef int8_t   I8;
typedef uint8_t  U8;
typedef int16_t  I16;
typedef uint16_t U16;
typedef int32_t  I32;
typedef uint32_t U32;
typedef int64_t  I64;
typedef uint64_t U64;


#define ASSERT(x) if (!(x)) *((volatile I64*)0) = (I64)0


#define LOG_ERROR(...) fprintf(stderr, __VA_ARGS__)

#define ALLOC(type, elements)            (type *)malloc(elements * sizeof(type))
#define REALLOC(type, old_ptr, elements) (type *)realloc(old_ptr, elements * sizeof(type))
#define FREE(ptr)                        free(ptr)

#define MIN(l, r) (((l) < (r)) ? l : r)
#define MAX(l, r) (((l) > (r)) ? l : r)



bool
Is_alpha(char c) {
    static const char alphas[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    I64 alphas_len = sizeof(alphas)-1;
    for (I64 i = 0; i<alphas_len; i+=1) {
        if (c == alphas[i]) return true;
    }
    return false;
}

bool
Is_space(char c) {
    static const char spaces[] = " \t\n\r";
    I64 spaces_len = sizeof(spaces)-1;
    for (I64 i = 0; i<spaces_len; i+=1) {
        if (c == spaces[i]) return true;
    }
    return false;
}

bool
Is_numeric(char c) {
    return (c >= '0' && c <= '9');
}

bool
Is_alphanumeric(char c) {
    if (Is_numeric(c)) return true;
    if (Is_alpha(c))   return true;
    return false;
}


typedef struct {
    char  *d;
    I64 len;
} String;

#define STR_LIT(s) (String){.d = s, .len = sizeof(s)-1}

static String
STR(char *data, I64 len) {
    String result;
    result.d   = data;
    result.len = len;
    return result;
}

static bool
STR_Equal(String l, String r) {
    if (l.len != r.len) return false;
    for (I64 i = 0; i < l.len; i+=1) {
        if (l.d[i] != r.d[i]) return false;
    }
    return true;
}

static String
Substring(String s, I64 begin, I64 end) {
    ASSERT(begin < end && begin >= 0 && end <= s.len);
    return STR(&s.d[begin], end-begin);
}

I64
STR_To_I64(String s) {
    I64 sign = 1;
    I64 result = 0;
    I64 i = 0;
    for (; i < s.len && Is_space(s.d[i]);   i+=1); // Trim spaces
    for (; i < s.len && s.d[i] == '-';      i+=1) sign *= -1;
    for (; i < s.len && Is_numeric(s.d[i]); i+=1) result = result*10+(I64)(s.d[i]-'0');
    for (; i < s.len && Is_space(s.d[i]);   i+=1); // Trim spaces
    ASSERT(i == s.len);
    return result*sign;
}


static String
Read_all_file(const char *filename) {
    String result = STR_LIT("");
    FILE *f = fopen(filename, "rb");
    if (!f) {
        LOG_ERROR("Error, cannot open file %s\n", filename);
        return result;
    }

    fseek(f, 0, SEEK_END);
    I64 data_size = ftell(f);
    fseek(f, 0, SEEK_SET);
    U8 *data = ALLOC(U8, data_size);
    if (!data) {
        LOG_ERROR("Out of memory\n");
        ASSERT(false);
    }
    I64 read_elements = fread(data, sizeof(U8), data_size, f);
    fclose(f);

    if (read_elements != data_size) {
        LOG_ERROR("Error, readed %ld elements, but expected %ld\n", data_size, read_elements);
    }

    result = STR((char *)data, read_elements);

    return result;
}

typedef bool (*QS_Less) (I32 l, I32 r);

static void
Quicksort_I32(I32 *arr, I32 len, QS_Less less) {
    if (len < 2) return;

    I32 pivot = arr[len/2];

    I32 i, j;
    for (i=0, j=len-1; ;i+=1, j-=1) {
        while (i < len && less(arr[i], pivot)) i+=1;
        while (0 < j   && less(pivot, arr[j])) j-=1;

        if (i >= j) break;

        I32 tmp = arr[i];
        arr[i]  = arr[j];
        arr[j]  = tmp;
    }

    Quicksort_I32(arr, i, less);
    Quicksort_I32(&arr[i], len-i, less);
}


typedef struct {
    U64 *hashes;
    U64 cap;
    U64 secret;
} HashMap;

static HashMap
Make_HashMap(U64 size, U64 secret) {
    HashMap result;
    result.hashes = ALLOC(U64, size);
    if (!result.hashes) {
        LOG_ERROR("Out of memory\n");
        ASSERT(false);
    }
    for (U64 i = 0; i < size; i+=1) {
        result.hashes[i] = 0;
    }
    result.cap    = size;
    result.secret = secret;

    return result;
}

static I64
HM_At_(HashMap *hm, const void *key, U64 key_size, bool insert);

static I64
HM_At(HashMap *hm, const void *key, U64 key_size, bool insert) {
    I64 result = HM_At_(hm, key, key_size, insert);
    return result;
}

static I64
HM_At_str(HashMap *hm, String s, bool insert) {
    return HM_At(hm, s.d, s.len, insert);
}

// This is free and unencumbered software released into the public domain under The Unlicense (http://unlicense.org/)
// main repo: https://github.com/wangyi-fudan/wyhash
// author: 王一 Wang Yi <godspeed_china@yeah.net>
// contributors: Reini Urban, Dietrich Epp, Joshua Haberman, Tommy Ettinger, Daniel Lemire, Otmar Ertl, cocowalla, leo-yuriev, Diego Barrios Romero, paulie-g, dumblob, Yann Collet, ivte-ms, hyb, James Z.M. Gao, easyaspi314 (Devin), TheOneric

/* quick example:
   string s="fjsakfdsjkf";
   uint64_t hash=wyhash(s.c_str(), s.size(), 0, _wyp);
*/

#ifndef wyhash_final_version_3
#define wyhash_final_version_3

#ifndef WYHASH_CONDOM
//protections that produce different results:
//1: normal valid behavior
//2: extra protection against entropy loss (probability=2^-63), aka. "blind multiplication"
#define WYHASH_CONDOM 1
#endif

#ifndef WYHASH_32BIT_MUM
//0: normal version, slow on 32 bit systems
//1: faster on 32 bit systems but produces different results, incompatible with wy2u0k function
#define WYHASH_32BIT_MUM 0  
#endif

//includes
#include <string.h>
#if defined(_MSC_VER) && defined(_M_X64)
  #include <intrin.h>
  #pragma intrinsic(_umul128)
#endif

//likely and unlikely macros
#if defined(__GNUC__) || defined(__INTEL_COMPILER) || defined(__clang__)
  #define _likely_(x)  __builtin_expect(x,1)
  #define _unlikely_(x)  __builtin_expect(x,0)
#else
  #define _likely_(x) (x)
  #define _unlikely_(x) (x)
#endif

//128bit multiply function
static inline uint64_t _wyrot(uint64_t x) { return (x>>32)|(x<<32); }
static inline void _wymum(uint64_t *A, uint64_t *B){
#if(WYHASH_32BIT_MUM)
  uint64_t hh=(*A>>32)*(*B>>32), hl=(*A>>32)*(uint32_t)*B, lh=(uint32_t)*A*(*B>>32), ll=(uint64_t)(uint32_t)*A*(uint32_t)*B;
  #if(WYHASH_CONDOM>1)
  *A^=_wyrot(hl)^hh; *B^=_wyrot(lh)^ll;
  #else
  *A=_wyrot(hl)^hh; *B=_wyrot(lh)^ll;
  #endif
#elif defined(__SIZEOF_INT128__)
  __uint128_t r=*A; r*=*B; 
  #if(WYHASH_CONDOM>1)
  *A^=(uint64_t)r; *B^=(uint64_t)(r>>64);
  #else
  *A=(uint64_t)r; *B=(uint64_t)(r>>64);
  #endif
#elif defined(_MSC_VER) && defined(_M_X64)
  #if(WYHASH_CONDOM>1)
  uint64_t  a,  b;
  a=_umul128(*A,*B,&b);
  *A^=a;  *B^=b;
  #else
  *A=_umul128(*A,*B,B);
  #endif
#else
  uint64_t ha=*A>>32, hb=*B>>32, la=(uint32_t)*A, lb=(uint32_t)*B, hi, lo;
  uint64_t rh=ha*hb, rm0=ha*lb, rm1=hb*la, rl=la*lb, t=rl+(rm0<<32), c=t<rl;
  lo=t+(rm1<<32); c+=lo<t; hi=rh+(rm0>>32)+(rm1>>32)+c;
  #if(WYHASH_CONDOM>1)
  *A^=lo;  *B^=hi;
  #else
  *A=lo;  *B=hi;
  #endif
#endif
}

//multiply and xor mix function, aka MUM
static inline uint64_t _wymix(uint64_t A, uint64_t B){ _wymum(&A,&B); return A^B; }

//endian macros
#ifndef WYHASH_LITTLE_ENDIAN
  #if defined(_WIN32) || defined(__LITTLE_ENDIAN__) || (defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
    #define WYHASH_LITTLE_ENDIAN 1
  #elif defined(__BIG_ENDIAN__) || (defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
    #define WYHASH_LITTLE_ENDIAN 0
  #else
    #warning could not determine endianness! Falling back to little endian.
    #define WYHASH_LITTLE_ENDIAN 1
  #endif
#endif

//read functions
#if (WYHASH_LITTLE_ENDIAN)
static inline uint64_t _wyr8(const uint8_t *p) { uint64_t v; memcpy(&v, p, 8); return v;}
static inline uint64_t _wyr4(const uint8_t *p) { uint32_t v; memcpy(&v, p, 4); return v;}
#elif defined(__GNUC__) || defined(__INTEL_COMPILER) || defined(__clang__)
static inline uint64_t _wyr8(const uint8_t *p) { uint64_t v; memcpy(&v, p, 8); return __builtin_bswap64(v);}
static inline uint64_t _wyr4(const uint8_t *p) { uint32_t v; memcpy(&v, p, 4); return __builtin_bswap32(v);}
#elif defined(_MSC_VER)
static inline uint64_t _wyr8(const uint8_t *p) { uint64_t v; memcpy(&v, p, 8); return _byteswap_uint64(v);}
static inline uint64_t _wyr4(const uint8_t *p) { uint32_t v; memcpy(&v, p, 4); return _byteswap_ulong(v);}
#else
static inline uint64_t _wyr8(const uint8_t *p) {
  uint64_t v; memcpy(&v, p, 8);
  return (((v >> 56) & 0xff)| ((v >> 40) & 0xff00)| ((v >> 24) & 0xff0000)| ((v >>  8) & 0xff000000)| ((v <<  8) & 0xff00000000)| ((v << 24) & 0xff0000000000)| ((v << 40) & 0xff000000000000)| ((v << 56) & 0xff00000000000000));
}
static inline uint64_t _wyr4(const uint8_t *p) {
  uint32_t v; memcpy(&v, p, 4);
  return (((v >> 24) & 0xff)| ((v >>  8) & 0xff00)| ((v <<  8) & 0xff0000)| ((v << 24) & 0xff000000));
}
#endif
static inline uint64_t _wyr3(const uint8_t *p, size_t k) { return (((uint64_t)p[0])<<16)|(((uint64_t)p[k>>1])<<8)|p[k-1];}
//wyhash main function
static inline uint64_t wyhash(const void *key, size_t len, uint64_t seed, const uint64_t *secret){
  const uint8_t *p=(const uint8_t *)key; seed^=*secret;	uint64_t	a,	b;
  if(_likely_(len<=16)){
    if(_likely_(len>=4)){ a=(_wyr4(p)<<32)|_wyr4(p+((len>>3)<<2)); b=(_wyr4(p+len-4)<<32)|_wyr4(p+len-4-((len>>3)<<2)); }
    else if(_likely_(len>0)){ a=_wyr3(p,len); b=0;}
    else a=b=0;
  }
  else{
    size_t i=len; 
    if(_unlikely_(i>48)){
      uint64_t see1=seed, see2=seed;
      do{
        seed=_wymix(_wyr8(p)^secret[1],_wyr8(p+8)^seed);
        see1=_wymix(_wyr8(p+16)^secret[2],_wyr8(p+24)^see1);
        see2=_wymix(_wyr8(p+32)^secret[3],_wyr8(p+40)^see2);
        p+=48; i-=48;
      }while(_likely_(i>48));
      seed^=see1^see2;
    }
    while(_unlikely_(i>16)){  seed=_wymix(_wyr8(p)^secret[1],_wyr8(p+8)^seed);  i-=16; p+=16;  }
    a=_wyr8(p+i-16);  b=_wyr8(p+i-8);
  }
  return _wymix(secret[1]^len,_wymix(a^secret[1],b^seed));
}

//the default secret parameters
static const uint64_t _wyp[4] = {0xa0761d6478bd642full, 0xe7037ed1a0b428dbull, 0x8ebc6af09c88c6e3ull, 0x589965cc75374cc3ull};

//a useful 64bit-64bit mix function to produce deterministic pseudo random numbers that can pass BigCrush and PractRand
static inline uint64_t wyhash64(uint64_t A, uint64_t B){ A^=0xa0761d6478bd642full; B^=0xe7037ed1a0b428dbull; _wymum(&A,&B); return _wymix(A^0xa0761d6478bd642full,B^0xe7037ed1a0b428dbull);}

//The wyrand PRNG that pass BigCrush and PractRand
static inline uint64_t wyrand(uint64_t *seed){ *seed+=0xa0761d6478bd642full; return _wymix(*seed,*seed^0xe7037ed1a0b428dbull);}

//convert any 64 bit pseudo random numbers to uniform distribution [0,1). It can be combined with wyrand, wyhash64 or wyhash.
static inline double wy2u01(uint64_t r){ const double _wynorm=1.0/(1ull<<52); return (r>>12)*_wynorm;}

//convert any 64 bit pseudo random numbers to APPROXIMATE Gaussian distribution. It can be combined with wyrand, wyhash64 or wyhash.
static inline double wy2gau(uint64_t r){ const double _wynorm=1.0/(1ull<<20); return ((r&0x1fffff)+((r>>21)&0x1fffff)+((r>>42)&0x1fffff))*_wynorm-3.0;}

#if(!WYHASH_32BIT_MUM)
//fast range integer random number generation on [0,k) credit to Daniel Lemire. May not work when WYHASH_32BIT_MUM=1. It can be combined with wyrand, wyhash64 or wyhash.
static inline uint64_t wy2u0k(uint64_t r, uint64_t k){ _wymum(&r,&k); return k; }
#endif

//make your own secret
static inline void make_secret(uint64_t seed, uint64_t *secret){
  uint8_t c[] = {15, 23, 27, 29, 30, 39, 43, 45, 46, 51, 53, 54, 57, 58, 60, 71, 75, 77, 78, 83, 85, 86, 89, 90, 92, 99, 101, 102, 105, 106, 108, 113, 114, 116, 120, 135, 139, 141, 142, 147, 149, 150, 153, 154, 156, 163, 165, 166, 169, 170, 172, 177, 178, 180, 184, 195, 197, 198, 201, 202, 204, 209, 210, 212, 216, 225, 226, 228, 232, 240 };
  for(size_t i=0;i<4;i++){
    uint8_t ok;
    do{
      ok=1; secret[i]=0;
      for(size_t j=0;j<64;j+=8) secret[i]|=((uint64_t)c[wyrand(&seed)%sizeof(c)])<<j;
      if(secret[i]%2==0){ ok=0; continue; }
      for(size_t j=0;j<i;j++) {
#if defined(__GNUC__) || defined(__INTEL_COMPILER) || defined(__clang__)
        if(__builtin_popcountll(secret[j]^secret[i])!=32){ ok=0; break; }
#elif defined(_MSC_VER) && defined(_M_X64)
        if(_mm_popcnt_u64(secret[j]^secret[i])!=32){ ok=0; break; }
#else
        //manual popcount
        uint64_t x = secret[j]^secret[i];
        x -= (x >> 1) & 0x5555555555555555;
        x = (x & 0x3333333333333333) + ((x >> 2) & 0x3333333333333333);
        x = (x + (x >> 4)) & 0x0f0f0f0f0f0f0f0f;
        x = (x * 0x0101010101010101) >> 56;
        if(x!=32){ ok=0; break; }
#endif
      }
    }while(!ok);
  }
}


/*  This is world's fastest hash map: 2x faster than bytell_hash_map.
    It does not store the keys, but only the hash/signature of keys.
    First we use pos=hash1(key) to approximately locate the bucket.
    Then we search signature=hash2(key) from pos linearly.
    If we find a bucket with matched signature we report the bucket
    Or if we meet a bucket whose signature=0, we report a new position to insert
    The signature collision probability is very low as we usually searched N~10 buckets.
    By combining hash1 and hash2, we acturally have 128 bit anti-collision strength.
    hash1 and hash2 can be the same function, resulting lower collision resistance but faster.
    The signature is 64 bit, but can be modified to 32 bit if necessary for save space.
    The above two can be activated by define WYHASHMAP_WEAK_SMALL_FAST
    simple examples:
    const	size_t	size=213432;
    vector<wyhashmap_t>	idx(size);	//	allocate the index of fixed size. idx MUST be zeroed.
    vector<value_class>	value(size);	//	we only care about the index, user should maintain his own value vectors.
    string  key="dhskfhdsj"	//	the object to be inserted into idx
    size_t	pos=wyhashmap(idx.data(), idx.size(), key.c_str(), key.size(), 1);	//	get the position and insert
    if(pos<size)	value[pos]++;	//	we process the vallue
    else	cerr<<"map is full\n";
    pos=wyhashmap(idx.data(), idx.size(), key.c_str(), key.size(), 0);	// just lookup by setting insert=0
    if(pos<size)	value[pos]++;	//	we process the vallue
    else	cerr<<"the key does not exist\n";
*/
/*
#ifdef	WYHASHMAP_WEAK_SMALL_FAST	// for small hashmaps whose size < 2^24 and acceptable collision
typedef	uint32_t	wyhashmap_t;
#else
typedef	uint64_t	wyhashmap_t;
#endif
static	inline	size_t	wyhashmap(wyhashmap_t	*idx,	size_t	idx_size,	const	void *key, size_t	key_size,	uint8_t	insert, uint64_t *secret){
	size_t	i=1;	uint64_t	h2;	wyhashmap_t	sig;
	do{	sig=h2=wyhash(key,key_size,i,secret);	i++;	}while(_unlikely_(!sig));
#ifdef	WYHASHMAP_WEAK_SMALL_FAST
	size_t	i0=wy2u0k(h2,idx_size);
#else
	size_t	i0=wy2u0k(wyhash(key,key_size,0,secret),idx_size);
#endif
	for(i=i0;	i<idx_size&&idx[i]&&idx[i]!=sig;	i++);
	if(_unlikely_(i==idx_size)){
		for(i=0;	i<i0&&idx[i]&&idx[i]!=sig;  i++);
		if(i==i0)	return	idx_size;
	}
	if(!idx[i]){
		if(insert)	idx[i]=sig;
		else	return	idx_size;
	}
	return	i;
}
*/
#endif

/* The Unlicense
This is free and unencumbered software released into the public domain.
Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.
In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
For more information, please refer to <http://unlicense.org/>
*/


static I64
HM_At_(HashMap *hm, const void *key, U64 key_size, bool insert) {
    U64 *idx     = hm->hashes;
    U64 idx_size = hm->cap;
    U64 *secret  = &hm->secret;
	U64 i=1;
    U64 h2;
    U64 sig;

	do {
        sig=h2=wyhash(key,key_size,i,secret);
        i+=1;
    }while(_unlikely_(!sig));

	U64 i0 = wy2u0k(wyhash(key,key_size,0,secret),idx_size);

	for(i=i0; i<idx_size && idx[i] && (idx[i]!=sig); i+=1);
	if (_unlikely_(i==idx_size)) {
		for(i=0; (i<i0) && (idx[i]) && (idx[i]!=sig); i+=1);
		if(i==i0) return -1;
	}

	if (!idx[i]) {
		if(insert)	idx[i]=sig;
		else return	-1;
	}
	return	(I64)i;
}


#include <stdint.h>

void
UT_Init(void);

int64_t
UT_Time(void);

#if defined(__linux__) || defined(__unix__)

#include <time.h>

static struct timespec UT__initial_time = {0};

void
UT_Init(void) {
	clock_gettime(CLOCK_MONOTONIC_RAW, &UT__initial_time);
}

int64_t
UT_Time(void) {
	const int64_t SECOND = 1000*1000*1000;
	struct timespec current;
	clock_gettime(CLOCK_MONOTONIC_RAW, &current);
	int64_t result = (current.tv_sec - UT__initial_time.tv_sec) * (1 * SECOND);
	result += current.tv_nsec - UT__initial_time.tv_nsec;
	return result;
}


#elif defined(_WIN32) || defined(WIN32)

#include <windows.h>

static LARGE_INTEGER UT__initial_time = {0};
static LARGE_INTEGER UT__time_freq = {0};

void
UT_Init(void) {
	QueryPerformanceFrequency(&UT__time_freq);
	QueryPerformanceCounter(&UT__initial_time);
}

int64_t
UT_Time(void) {
	const int64_t SECOND = 1000*1000*1000;

	LARGE_INTEGER qpc_t;
	QueryPerformanceCounter(&qpc_t);

	int64_t counter_diff = qpc_t.QuadPart - UT__initial_time.QuadPart;
	int64_t freq         = UT__time_freq.QuadPart;
	
	// This is to preserve the precission (from sokol time)
    int64_t q = counter_diff / freq;
    int64_t r = counter_diff % freq;

    int64_t result = q * SECOND + (r * SECOND) / freq;
	return result;
}

#elif defined (__wasm__)

#define UT__WA_JS(ret, name, args, ...) extern __attribute__((import_module("JS"), import_name(#name "|" #args "|" #__VA_ARGS__))) ret name args;

static int64_t UT__initial_time = 0;

UT__WA_JS(double, UT__JS_Get_time, (void), {
	return Date.now();
});

void
UT_Init(void) {
	UT__initial_time = (int64_t)UT__JS_Get_time();
}

int64_t
UT_Time(void) {
	int64_t MSECOND = 1000 * 1000;
	int64_t result = MSECOND*((int64_t)UT__JS_Get_time() - UT__initial_time);
	return result;
}

#endif
